kube.%: KUBECTL_ENDPOINT?=http://192.168.98.101:8080
kube.%: KUBECTL?=$(CURDIR)/bin/kubectl -s $(KUBECTL_ENDPOINT)
kube.%: GET_NS=$(firstword $(subst ., ,$(1)))
kube.%: GET_NAME=$(subst $(firstword $(subst ., ,$(1))).,,$(1))
kube.%: NAMESPACE?=$(or $(call GET_NS, $*),default)
kube.%: NAME?=$(call GET_NAME, $*)

kube.test.%:
	echo Namespace:$(NAMESPACE) Name:/$(NAME)/

.PHONY: kube.nodes
DESCRIPTION+="kube.nodes: List all Cluster Nodes"
kube.nodes:
	$(KUBECTL) get nodes

.PHONY: kube.node.info. kube.node.info.%
DESCRIPTION+="kube.node.info.<NAME>: Get Node Info"
kube.node.info.%:
	$(KUBECTL) describe node $*

.PHONY: kube.node.edit. kube.node.edit.%
DESCRIPTION+="kube.node.edit.<NAME>: Edit Node Info"
kube.node.edit.%:
	$(KUBECTL) edit node $*

.PHONY: kube.create
DESCRIPTION+="kube.create <FILE>: Submit a FILE to the cluster"
kube.create:
	$(KUBECTL) create -f $(FILE)

.PHONY: kube.update
DESCRIPTION+="kube.update <FILE>: Update a FILE into the cluster"
kube.update:
	$(KUBECTL) update -f $(FILE)

.PHONY: kube.services
DESCRIPTION+="kube.services: List all Services"
kube.services:
	$(KUBECTL) get svc --all-namespaces

.PHONY: kube.service.info. kube.service.info.%
DESCRIPTION+="kube.service.info.[NS].<NAME>: Get Service Info"
kube.service.info.%:
	$(KUBECTL) describe svc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.service.edit. kube.service.edit.%
DESCRIPTION+="kube.service.edit.[NS].<NAME>: Edit Service Info"
kube.service.edit.%:
	$(KUBECTL) edit svc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.service.cat. kube.service.cat.%
DESCRIPTION+="kube.service.cat.[NS].<NAME>: Get Service"
kube.service.cat.%:
	$(KUBECTL) get svc $(NAME) --namespace=$(NAMESPACE) -o yaml

.PHONY: kube.service.rm. kube.service.rm.%
DESCRIPTION+="kube.service.rm.[NS].<NAME>: Delete a service"
kube.service.rm.%:
	$(KUBECTL) delete svc $(NAME) --namespace=$(NAMESPACE)


.PHONY: kube.rc
DESCRIPTION+="kube.rc: List all Replication Controllers"
kube.rc:
	$(KUBECTL) get rc --all-namespaces

.PHONY: kube.rc.info. kube.rc.info.%
DESCRIPTION+="kube.rc.info.[NS].<NAME>: Get Replication Controller Info"
kube.rc.info.%:
	$(KUBECTL) describe rc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.rc.edit. kube.rc.edit.%
DESCRIPTION+="kube.rc.edit.[NS].<NAME>: Edit Replication Controller Info"
kube.rc.edit.%:
	$(KUBECTL) edit rc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.rc.cat. kube.rc.cat.%
DESCRIPTION+="kube.rc.cat.[NS].<NAME>: Get Replication Controller"
kube.rc.cat.%:
	$(KUBECTL) get rc $(NAME) --namespace=$(NAMESPACE) -o yaml

.PHONY: kube.rc.rm. kube.rc.rm.%
DESCRIPTION+="kube.rc.rm.[NS].<NAME>: Delete a Replication Controller"
kube.rc.rm.%:
	$(KUBECTL) delete rc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.rc.scale. kube.rc.scale.%
DESCRIPTION+="kube.rc.scale.[NS].<NAME> <REPLICAS>: Scale a Replication Controller"
kube.rc.scale.%:
	$(KUBECTL) scale --replicas=$(REPLICAS) rc $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.rc.rolling-update. kube.rc.rolling-update.%
DESCRIPTION+="kube.rc.rolling-update.[NS].<NAME> <FILE>: Rolling Update a Replication Controller"
kube.rc.rolling-update.%: OPTS?=--update-period="10s"
kube.rc.rolling-update.%:
	$(KUBECTL) rolling-update $(NAME) -f $(FILE) $(OPTS) --namespace=$(NAMESPACE)


.PHONY: kube.pods
DESCRIPTION+="kube.pods: List all Cluster Pods"
kube.pods:
	$(KUBECTL) get pods -o wide --all-namespaces

.PHONY: kube.pod.info. kube.pod.info.%
DESCRIPTION+="kube.pod.info.[NS].<NAME>: Get POD Info"
kube.pod.info.%:
	$(KUBECTL) describe pod $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.pod.edit. kube.pod.edit.%
DESCRIPTION+="kube.pod.edit.[NS].<NAME>: Edit POD Info"
kube.pod.edit.%:
	$(KUBECTL) edit pod $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.pod.cat. kube.pod.cat.%
DESCRIPTION+="kube.pod.cat.[NS].<NAME>: Get POD"
kube.pod.cat.%:
	$(KUBECTL) get pod $(NAME) -o yaml --namespace=$(NAMESPACE)

.PHONY: kube.pod.log. kube.pod.log.%
DESCRIPTION+="kube.pod.log.[NS].<NAME>: Show POD logs"
kube.pod.log.%: OPTS?=-f --tail 100
kube.pod.log.%:
	$(KUBECTL) logs $(OPTS) $(NAME) --namespace=$(NAMESPACE)

.PHONY: kube.pod.label. kube.pod.label.%
DESCRIPTION+="kube.pod.label.[NS].<NAME> <LABEL>: Set a POD label"
kube.pod.label.%: OPTS?=--overwrite
kube.pod.label.%:
	$(KUBECTL) label pod $(NAME) $(LABEL) $(OPTS) --namespace=$(NAMESPACE)

.PHONY: kube.pod.exec. kube.pod.exec.%
DESCRIPTION+="kube.pod.exec.[NS].<NAME>: Exec a command inside a POD"
kube.pod.exec.%: CMD?=sh
kube.pod.exec.%: OPTS?=-ti
kube.pod.exec.%:
		$(KUBECTL) exec $(NAME) --namespace=$(NAMESPACE) $(OPTS) -- $(CMD)

.PHONY: kube.pod.shell. kube.pod.shell.%
DESCRIPTION+="kube.pod.shell.[NS].<NAME>: Exec a command inside a POD"
kube.pod.shell.%: CMD?=sh
kube.pod.shell.%: kube.pod.exec.%
	$(NOP)

.PHONY: kube.pod.rm. kube.pod.rm.%
DESCRIPTION+="kube.pod.rm.<NAME>: Delete a POD"
kube.pod.rm.%:
	$(KUBECTL) delete pod $(NAME) --namespace=$(NAMESPACE)
