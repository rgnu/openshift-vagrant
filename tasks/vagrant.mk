vagrant.%: VAGRANT?=vagrant


.PHONY: vagrant.ssh
DESCRIPTION+="vagrant.ssh: Log into node 1"
vagrant.ssh: vagrant.node.ssh.01


.PHONY: vagrant.node.ssh.%
DESCRIPTION+="vagrant.node.ssh.<NODE>: Log into node"
vagrant.node.ssh.%:
	$(VAGRANT) ssh origin-node$* -- -A


.PHONY: vagrant.node.start.%
DESCRIPTION+="vagrant.node.start.<NODE>: Start a new node"
vagrant.node.start.%:
	$(VAGRANT) up origin-node$*

.PHONY: vagrant.node.stop.%
DESCRIPTION+="vagrant.node.stop.<NODE>: Stop a node"
vagrant.node.stop.%:
	$(VAGRANT) halt origin-node$*

.PHONY: vagrant.node.rm.%
DESCRIPTION+="vagrant.node.rm.<NODE>: Remove a node"
vagrant.node.rm.%:
	$(VAGRANT) destroy origin-node$*
