
.PHONY: node.start. node.start.%
DESCRIPTION+="node.start.<NODE>: Alias for vagrant.node.start.<NODE>"
node.start.%: vagrant.node.start.%
	$(NOP)

.PHONY: node.stop. node.stop.%
DESCRIPTION+="node.stop.<NODE>: Alias for vagrant.node.stop.<NODE>"
node.stop.%: vagrant.node.stop.%
	$(NOP)

.PHONY: node.restart. node.restart.%
DESCRIPTION+="node.restart.<NODE>: Restart a node"
node.restart.%: vagrant.node.stop.% vagrant.node.start.%
	$(NOP)

.PHONY: node.shell. node.shell.%
DESCRIPTION+="node.shell.<NODE>: Init a shell into <NODE>"
node.shell.%: vagrant.node.ssh.%
	$(NOP)

.PHONY: node.rm. node.rm.%
DESCRIPTION+="node.rm.<NODE>: Alias for vagrant.node.rm.<NODE>"
node.rm.%: vagrant.node.rm.%
	$(NOP)
