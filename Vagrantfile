# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'fileutils'

Vagrant.require_version ">= 1.6.0"

CURDIR = File.dirname(__FILE__)


# Defaults for config options defined in CONFIG
$num_instances         = 5
$instance_name_prefix  = "origin-node"
$vm_memory             = 1024
$vm_cpus               = 2

Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/trusty64"
  config.landrush.enabled = true


  (1..$num_instances).each do |i|
    config.vm.define vm_name = "%s%02d" % [$instance_name_prefix, i] do |config|
      config.vm.hostname = "#{vm_name}.vagrant.test"

      config.vm.provider :virtualbox do |vb|
        vb.memory = $vm_memory
        vb.cpus   = $vm_cpus
      end

      ip = "192.168.98.#{i+100}"
      config.vm.network :private_network, ip: ip

      config.vm.synced_folder ".", "/vagrant"

#      config.vm.provision :file, :source => "#{CURDIR}/kubernetes.tar.gz", :destination => "/tmp/kubernetes.tar.gz"

      config.vm.provision "shell", inline: <<-SHELL

        sudo cp -ar /vagrant/bin /usr/local/
        sudo cp -ar /vagrant/etc /
        sudo cp -ar /vagrant/opt /

        sudo mkdir -p /var/lib/etcd

        # Add IP
        echo 'PRIVATE_IPV4=#{ip}' | sudo tee -a /etc/environment

        # Add MASTER
        echo 'KUBERNETES_MASTER=192.168.98.101' | sudo tee -a /etc/environment

        # Add DNS Config
        echo 'KUBERNETES_DNS_IP=172.17.0.1' | sudo tee -a /etc/environment
        echo 'KUBERNETES_DNS_DOMAIN=cluster.local' | sudo tee -a /etc/environment

        # Add NODES
        echo 'KUBERNETES_NODES=#{$num_instances}' | sudo tee -a /etc/environment
        echo 'KUBERNETES_NODE_ID=#{i}' | sudo tee -a /etc/environment

        echo 'KUBERNETES_SERVICE_CLUSTER_IP_RANGE=172.30.0.0/16' | sudo tee -a /etc/environment


        # Update Packages
        sudo apt-get update
        sudo apt-get install apt-transport-https ca-certificates
        sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80  --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
        echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
        sudo apt-get update
        # Install Docker
        sudo apt-get install --yes --force-yes -o Dpkg::Options::="--force-confold" docker-engine

        # Start Service
        #sudo service flanneld start
        #sudo service docker restart
      SHELL
    end
  end
end
